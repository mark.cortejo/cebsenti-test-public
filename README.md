# Cebu Pacific Categorization API Server

## Pre-requisites

- Have python 3.7 installed
- Have virtualenv installed
- Have nodeJS v.10 installed

## To Install

Run on virtualenv

```
virtualenv env --python [Path to python3.7]
source env/bin/activate
pip install -r requirements.txt
npm i
```

If there are audit errors from npm
```
npm audit fix
```

## Why install npm?

This is for the commitizen to work and for us to follow a consistent way of commiting. You can read more at CONTRIBUTING.md on the section `Commit`


## Structure

We follow a modified structure of https://www.digitalocean.com/community/tutorials/how-to-structure-large-flask-applications and https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-xv-a-better-application-structure

We used this structure for versioning: https://stackoverflow.com/questions/28795561/support-multiple-api-versions-in-flask

This is coming from the Senti Natter new branch version (request for access if needed)

All app code should be in the `app` folder

The `app` folder is as follows

```
- app
  - api
    - base
      - __init__.py
      - routes.py
    - v1
      - __init__.py
      - routes.py
    - v(n) // other versions of the api
      - __init__.py
      - routes.py
  - errors
    - __init__.py
    - handlers.py
  - modules
    - module.1
      - v(n)
        - __init__.py // this is where you put the function that will be called to process data in routes
  - static // if you need static files
  - templates // if you need html templates
- config.py // configuration file to be used to run the app
- run.py // run the file
- Dockerfile // a base dockerfile this is the stable number of gunicorn workers
```

More documentation to follow
