tensorflow==2.0.0-beta1
numpy==1.15.1
Flask
keras
scikit-learn
nltk
flask_restplus==0.12.1
fasttext