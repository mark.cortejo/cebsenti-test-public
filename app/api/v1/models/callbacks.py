import tensorflow as tf
import datetime
import numpy as np
from tensorflow.keras.callbacks import Callback
from sklearn.metrics import f1_score, precision_score, recall_score

class LogFilePrintingCallback(Callback):   
    """
    Got this code from https://www.tensorflow.org/beta/guide/keras/custom_callback#usage_of_logs_dict
    
    Log loss and accuracy to file after every training end event
    """
    def on_train_batch_end(self, batch, logs=None):
#         if batch % 10:
#             with open('logs/logs_batch_{}.txt'.format(batch), 'a+') as f:
#                 log_message = ""
#                 for metric, value in logs.items():
#                     log_message = log_message + metric+ " : "+str(value) +"|"
#                 f.write("For batch {}, the metrics are {} \n".format(batch, log_message))
#             f.close()
            
        with open('logs/logs_current_batch.txt', 'w+') as f:
            log_message = ""
            for metric, value in logs.items():
                log_message = log_message + metric+ " : "+str(value) +"|"
            f.write("For batch {}, the metrics are {} \n".format(batch, log_message))
        f.close()
        
    def on_epoch_end(self, epoch, logs=None):
        with open('logs/logs_last_epoch.txt', 'w+') as f:
            log_message = ""
            for metric, value in logs.items():
                log_message = log_message + metric+ " : "+str(value) +"|"
            f.write("For epoch {}, the metrics are {} \n".format(epoch, log_message))
        f.close()
    
#     def on_test_batch_end(self, batch, logs=None):
#         with open('logs/logs_test_{}.txt'.format(batch), 'a+') as f:
#             log_message = ""
#             for metric, value in logs.items():
#                 log_message = log_message + metric+ " : "+str(value) +"|"
#             f.write("For batch {}, the metrics are {} \n".format(batch, log_message))
#         f.close()
#         print('Evaluating: batch {} ends at {}'.format(batch, datetime.datetime.now().time()))


class EarlyStoppingAtMinLoss(Callback):
    """
      Got this code from https://www.tensorflow.org/beta/guide/keras/custom_callback#early_stopping_at_minimum_loss
      Stop training when the loss is at its min, i.e. the loss stops decreasing.

      Arguments:
          patience: Number of epochs to wait after min has been hit. After this
          number of no improvement, training stops.
    """
    def __init__(self, patience=0):
        super(EarlyStoppingAtMinLoss, self).__init__()

        self.patience = patience

        # best_weights to store the weights at which the minimum loss occurs.
        self.best_weights = None

    def on_train_begin(self, logs=None):
        # The number of epoch it has waited when loss is no longer minimum.
        self.wait = 0
        # The epoch the training stops at.
        self.stopped_epoch = 0
        # Initialize the best as infinity.
        self.best = np.Inf

    def on_epoch_end(self, epoch, logs=None):
        current = logs.get('loss')
        if np.less(current, self.best):
            self.best = current
            self.wait = 0
                # Record the best weights if current results is better (less).
            self.best_weights = self.model.get_weights()
        else:
            self.wait += 1
            if self.wait >= self.patience:
                self.stopped_epoch = epoch
                self.model.stop_training = True
                print('Restoring model weights from the end of the best epoch.')
                self.model.set_weights(self.best_weights)

    def on_train_end(self, logs=None):
        if self.stopped_epoch > 0:
            print('Epoch %05d: early stopping' % (self.stopped_epoch + 1))
            
class Metrics(Callback):
    def on_train_begin(self, logs={}):
        self.val_f1s = []
        self.val_recalls = []
        self.val_precisions = []

    def on_epoch_end(self, epoch, logs={}):
        val_predict = (np.asarray(self.model.predict(self.model.validation_data[0]))).round()
        val_targ = self.model.validation_data[1]
        _val_f1 = f1_score(val_targ, val_predict)
        _val_recall = recall_score(val_targ, val_predict)
        _val_precision = precision_score(val_targ, val_predict)
        self.val_f1s.append(_val_f1)
        self.val_recalls.append(_val_recall)
        self.val_precisions.append(_val_precision)
        print(" -” val_f1: {} -” val_precision: {} -” val_recall {}".format(_val_f1, _val_precision, _val_recall))
        return
    
class LossHistory(Callback):
    def on_train_begin(self, logs={}):
        self.val_loss_trace = []
        self.val_acc_trace = []
        self.train_loss_trace = []
        self.train_acc_trace = []

    def on_epoch_end(self, batch, logs={}):
        self.val_loss_trace.append(logs.get('val_loss'))
        self.val_acc_trace.append(logs.get('val_categorical_accuracy'))
        self.train_loss_trace.append(logs.get('loss'))
        self.train_acc_trace.append(logs.get('categorical_accuracy'))