from tensorflow import keras
import numpy as np
from .utils import *
# def load_model():
# #prediction test
# 	#load class indeces
	# tof_index = load_tof_index()
	# complaint_index = load_complaint_index()
	# inquiry_index = load_inquiry_index()
	# request_index = load_request_index()
# 	#load Max sequence length for the tokenizer
MAX_SEQUENCE_LENGTH = 1000
	#load the tokenizers
tokenize = Load.tokenize()

# tokenizetof = tokenize_tof()
# tokenizecategory = tokenize_category()
# 	#load the models
# 	tof_mod = tof_load_model()
# 	complaint_mod = complaint_load_model()
# 	inquiry_mod = inquiry_load_model()
# 	request_mod = request_load_model()
class Predict:
	def predict(self, input_data, tof_model, complaint_model, inquiry_model, request_model, complaint_airport_experience_model, 
		complaint_booking_experience_model, complaint_flight_disruption_model, 
		complaint_getgo_model, complaint_inflight_experience_model,
		complaint_refund_model, inquiry_booking_inquiry_model, inquiry_cargo_related_model,
		inquiry_manage_booking_model, request_common_waiver, request_flight_cancellation_changes_options,
		request_itinerary_receipt, request_name_correction, request_official_receipt, request_passenger_contact_details):
		
		tof_index, inquiry_index, complaint_index,request_index, 
		complaint_airport_experience_index, complaint_booking_experience_index, 
		complaint_flight_disruption_index, complaint_refund_index, complaint_getgo_index,
		complaint_inflight_experience_index, inquiry_booking_inquiry_index, inquiry_cargo_related_index,
		inquiry_manage_booking_index, request_common_waiver_index, request_flight_cancellation_changes_options_index,
		request_itinerary_receipt_index, request_name_correction_index,
		request_official_receipt_index, request_passenger_contact_details_index = load.load_index()
		#layer 1 tokenize
		test_sequence = tokenize.texts_to_sequences([input_data])
		test_sequence = keras.preprocessing.sequence.pad_sequences(test_sequence, MAX_SEQUENCE_LENGTH)
		#layer 1 predict
		pred = tof_model.predict(test_sequence)
		result_tof = [key for key, val in tof_index.items() if val == np.argmax(pred)]
		
		#layer 2
		if result_tof[0]=='Complaint':
			test_sequence = tokenize.texts_to_sequences([input_data])
			test_sequence = keras.preprocessing.sequence.pad_sequences(test_sequence, MAX_SEQUENCE_LENGTH)
			pred = complaint_model.predict(test_sequence)
			result_category = [key for key, val in complaint_index.items() if val == np.argmax(pred)]
			if result_category[0]=='Airport Experience':
				test_sequence = tokenize.texts_to_sequences([input_data])
				test_sequence = keras.preprocessing.sequence.pad_sequences(test_sequence, MAX_SEQUENCE_LENGTH)
				pred = complaint_airport_experience_model.predict(test_sequence)
				result_layer_3 = [key for key, val in complaint_airport_experience_index.items() if val == np.argmax(pred)]
			elif result_category[0]=='Flight Disruption':
				test_sequence = tokenize.texts_to_sequences([input_data])
				test_sequence = keras.preprocessing.sequence.pad_sequences(test_sequence, MAX_SEQUENCE_LENGTH)
				pred = complaint_flight_disruption_model.predict(test_sequence)
				result_layer_3 = [key for key, val in complaint_flight_disruption_index.items() if val == np.argmax(pred)]
			elif result_category[0]=='Booking Experience':
				test_sequence = tokenize.texts_to_sequences([input_data])
				test_sequence = keras.preprocessing.sequence.pad_sequences(test_sequence, MAX_SEQUENCE_LENGTH)
				pred = complaint_booking_experience_model.predict(test_sequence)
				result_layer_3 = [key for key, val in complaint_booking_experience_index.items() if val == np.argmax(pred)]
			elif result_category[0]=='Refund':
				test_sequence = tokenize.texts_to_sequences([input_data])
				test_sequence = keras.preprocessing.sequence.pad_sequences(test_sequence, MAX_SEQUENCE_LENGTH)
				pred = complaint_refund_model.predict(test_sequence)
				result_layer_3 = [key for key, val in complaint_refund_index.items() if val == np.argmax(pred)]
			elif result_category[0]=='GetGo':
				test_sequence = tokenize.texts_to_sequences([input_data])
				test_sequence = keras.preprocessing.sequence.pad_sequences(test_sequence, MAX_SEQUENCE_LENGTH)
				pred = complaint_getgo_model.predict(test_sequence)
				result_layer_3 = [key for key, val in complaint_getgo_index.items() if val == np.argmax(pred)]
			elif result_category[0]=='Inflight Experience':
				test_sequence = tokenize.texts_to_sequences([input_data])
				test_sequence = keras.preprocessing.sequence.pad_sequences(test_sequence, MAX_SEQUENCE_LENGTH)
				pred = complaint_inflight_experience_model.predict(test_sequence)
				result_layer_3 = [key for key, val in complaint_inflight_experience_model.items() if val == np.argmax(pred)]
			
		elif result_tof[0]=='Inquiry':
			test_sequence = tokenize.texts_to_sequences([input_data])
			test_sequence = keras.preprocessing.sequence.pad_sequences(test_sequence, MAX_SEQUENCE_LENGTH)
			pred = inquiry_model.predict(test_sequence)
			result_category = [key for key, val in inquiry_index.items() if val == np.argmax(pred)]
			if result_category[0]=='Booking Inquiry':
				test_sequence = tokenize.texts_to_sequences([input_data])
				test_sequence = keras.preprocessing.sequence.pad_sequences(test_sequence, MAX_SEQUENCE_LENGTH)
				pred = inquiry_booking_inquiry_index.predict(test_sequence)
				result_category = [key for key, val in inquiry_booking_inquiry_index.items() if val == np.argmax(pred)]
			elif result_category[0]=='Cargo Related':
				test_sequence = tokenize.texts_to_sequences([input_data])
				test_sequence = keras.preprocessing.sequence.pad_sequences(test_sequence, MAX_SEQUENCE_LENGTH)
				pred = inquiry_cargo_related_model.predict(test_sequence)
				result_layer_3 = [key for key, val in inquiry_cargo_related_index.items() if val == np.argmax(pred)]
			elif result_category[0]=='Manage Booking':
				test_sequence = tokenize.texts_to_sequences([input_data])
				test_sequence = keras.preprocessing.sequence.pad_sequences(test_sequence, MAX_SEQUENCE_LENGTH)
				pred = inquiry_manage_booking_model.predict(test_sequence)
				result_layer_3 = [key for key, val in inquiry_manage_booking_index.items() if val == np.argmax(pred)]
			 
			
		elif result_tof[0]=='Request':
			test_sequence = tokenize.texts_to_sequences([input_data])
			test_sequence = keras.preprocessing.sequence.pad_sequences(test_sequence, MAX_SEQUENCE_LENGTH)
			pred = request_model.predict(test_sequence)
			result_category = [key for key, val in inquiry_index.items() if val == np.argmax(pred)]
			if result_category[0]=='Common Waiver':
				test_sequence = tokenize.texts_to_sequences([input_data])
				test_sequence = keras.preprocessing.sequence.pad_sequences(test_sequence, MAX_SEQUENCE_LENGTH)
				pred = request_common_waiver.predict(test_sequence)
				result_category = [key for key, val in request_common_waiver.items() if val == np.argmax(pred)]
			elif result_category[0]=='Flight Cancellation':
				test_sequence = tokenize.texts_to_sequences([input_data])
				test_sequence = keras.preprocessing.sequence.pad_sequences(test_sequence, MAX_SEQUENCE_LENGTH)
				pred = request_flight_cancellation_changes_options.predict(test_sequence)
				result_layer_3 = [key for key, val in request_flight_cancellation_changes_options_index.items() if val == np.argmax(pred)]
			elif result_category[0]=='Itenerary Receipt':
				test_sequence = tokenize.texts_to_sequences([input_data])
				test_sequence = keras.preprocessing.sequence.pad_sequences(test_sequence, MAX_SEQUENCE_LENGTH)
				pred = request_itinerary_receipt.predict(test_sequence)
				result_layer_3 = [key for key, val in request_itinerary_receipt_index.items() if val == np.argmax(pred)]
			elif result_category[0]=='Name Correction':
				test_sequence = tokenize.texts_to_sequences([input_data])
				test_sequence = keras.preprocessing.sequence.pad_sequences(test_sequence, MAX_SEQUENCE_LENGTH)
				pred = request_name_correction.predict(test_sequence)
				result_layer_3 = [key for key, val in request_name_correction_index.items() if val == np.argmax(pred)]
			elif result_category[0]=='Official Receipt':
				test_sequence = tokenize.texts_to_sequences([input_data])
				test_sequence = keras.preprocessing.sequence.pad_sequences(test_sequence, MAX_SEQUENCE_LENGTH)
				pred = request_official_receipt.predict(test_sequence)
				result_layer_3 = [key for key, val in request_official_receipt_index.items() if val == np.argmax(pred)]
			elif result_category[0]=='Passenger/Contact Details':
				test_sequence = tokenize.texts_to_sequences([input_data])
				test_sequence = keras.preprocessing.sequence.pad_sequences(test_sequence, MAX_SEQUENCE_LENGTH)
				pred = request_passenger_contact_details.predict(test_sequence)
				result_layer_3 = [key for key, val in request_passenger_contact_details_index.items() if val == np.argmax(pred)]
			
		# print("CONFIDENCE", pred[np.argmax(pred)])
		return result_tof, result_category, result_layer_3