from flask import Blueprint, request, jsonify
from flask_restplus import Api, Resource
from .classifier import Predict

api = Blueprint('classify_api',__name__)
from app.api.v1.utils import Load

load = Load()
predict = Predict()
'''
load the models at initialization

'''
tof_model, complaint_model, inquiry_model, request_model, complaint_airport_experience_model, complaint_booking_experience_model, complaint_flight_disruption_model, complaint_getgo_model, complaint_inflight_experience_model, complaint_refund_model, inquiry_booking_inquiry_model, inquiry_cargo_related_model, inquiry_manage_booking_model, request_common_waiver_model, request_flight_cancellation_changes_options, request_itinerary_receipt, request_name_correction, request_official_receipt, request_passenger_contact_details = load.load_model()


# tokenize = load.tokenize()
@api.route("/classifications/", methods=['POST'])
def post():
    """
    returns language classified
    """
    data = request.get_json()
    text = data['text']
    result_tof, result_category, result_layer_3 = predict.predict(text, tof_model, complaint_model, inquiry_model, request_model, tof_index, inquiry_index , complaint_index, request_index)
    return jsonify(success=True, type_of_feedback=result_tof, category=result_category, sub_category_1=result_layer_3)
    