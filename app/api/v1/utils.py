from tensorflow import keras
from .models.RNNModel import RNN
import pickle
from .utilities.input import Tokenizer
MODEL_NAME = 'layer1_tof'

class Load:
	def load_index(self):

		tof_index = {
		"Inquiry": 0,
		"Complaint": 1,
		"Request": 2,
		}
		complaint_index = {
			 "Booking Experience": 0,
			 "Airport Experience": 1,
			 "Flight Disruption": 2,
			 "Refund": 3,
			 "Negative Feedback": 4,
			 "GetGo": 5,
			 "Products and Business Rules": 6,
			 "Inflight Experience": 7,
			 "Fraudulent Transaction": 8
		}
		inquiry_index = {
			 "Booking/Payment Status": 0,
			 "Fares, Fees and Schedule": 1,
			 "Flight Status and Flight Disruption": 2,
			 "Manage Booking": 3,
			 "Check-in Baggage/FBA": 4,
			 "Booking Inquiry": 5,
			 "GetGo Loyalty Program": 6,
			 "Travel Documents": 7,
			 "Special Handling": 8,
			 "Job Opportunity": 9,
			 "Cargo Related": 10,
			 "Cadet Pilot Program": 11,
			 "Travel Agency Accreditation": 12
		}
		request_index = {
			"Name Correction": 0,
			"Flight Cancellation/Changes Options": 1,
			"Itinerary Receipt": 2,
			"Passenger/Contact Details": 3,
			"Double or Multiple Booking of the same guest (DB)": 4,
			"Official Receipt": 5,
			"Travel Voucher Redemption": 6,
			"Certificate/Letter for Cancelled/Delayed Flight": 7,
			"Certificate of Travel/No Show": 8,
			"Special Waivers": 9,
			"Delayed Itinerary Sending": 10,
			"Common Waiver": 11,
			"Double or Multiple Payment of Booking (DP)": 12,
			"Special Service Request (SSR)": 13,
			"Travel Fund (Voluntary)": 14,
		}
		complaint_airport_experience_index = {
			'Baggage Handling': 0, 
			'Boarding': 2, 
			'Bag drop / Check-in': 1
		}

		complaint_booking_experience_index = {
			'Website': 0, 
			'Call Center': 1, 
			'Mobile App': 2, 
			'Ticketing': 3
		}
		complaint_flight_disruption_index = {
			'Handling': 1, 
			'Schedule Change': 0
		}

		complaint_refund_index = {
			'Beyond 10 working days': 1, 
			'Within 10 working days': 0
		}

		complaint_getgo_index = {
			'Cannot Log-in': 0, 
			'Missing Points': 1}

		complaint_inflight_experience_index = {
			'Meal not served': 0, 
			'Aircraft amenities': 2, 
			'Cabin crew': 1
		}

		inquiry_booking_inquiry_index = {
			'Where to Book': 3, 
			'How to Book': 0, 
			'Where to Pay': 2, 
			'How to Pay': 1
		}

		inquiry_cargo_related_index = {
			'Contact Information': 1, 
			'Contact Details': 2, 
			'AVI': 0, 
			'Human Remains': 3
		}

		inquiry_manage_booking_index = {
			'Digital Check-In': 2, 
			'Booking Changes': 0, 
			'Infant': 3, 
			'Ancillary Add-ons': 1
		}

		request_common_waiver_index = {
			'Immigration Profiling': 0, 
			'Late Check-In': 1, 
			'Insufficient Documents': 2
		}

		request_flight_cancellation_changes_options_index = {
			'Refund': 1, 
			'Rebook': 0, 
			'Accept': 2, 
			'Travel Fund': 4, 
			'Reroute': 3
		}

		request_itinerary_receipt_index = {
			'No itinerary sent': 1, 
			'Resending': 0
		}

		request_name_correction_index = {
			'Correction of Title': 5, 
			'Correction of Maiden Name to Married Name and vv': 3, 
			'Booking Changes': 0, 
			'Correction of Sounds-like Name': 10, 
			'Correction of Nickname to Real Name': 9, 
			'Minor Spelling Mistakes': 1, 
			'Change of Name Arrangement': 7, 
			'Correction of Middle Name to Last Name': 6, 
			'Correction of Erroneously Inputted Name (Web Only)': 2, 
			'Addition or Deletion of Second Name': 4, 
			'Addition or Deletion of Suffix': 8
		}

		request_official_receipt_index = {
			'Automated': 0, 
			'Manual': 1
		}

		request_passenger_contact_details_index = {
			'Contact Details': 2, 
			'DOB': 0, 
			'GetGo Membership Number': 3, 
			'Passport': 1
		}
		return tof_index, inquiry_index, complaint_index,request_index, 
		complaint_airport_experience_index, complaint_booking_experience_index, 
		complaint_flight_disruption_index, complaint_refund_index, complaint_getgo_index,
		complaint_inflight_experience_index, inquiry_booking_inquiry_index, inquiry_cargo_related_index,
		inquiry_manage_booking_index, request_common_waiver_index, request_flight_cancellation_changes_options_index,
		request_itinerary_receipt_index, request_name_correction_index,
		request_official_receipt_index, request_passenger_contact_details_index

	def load_model(self):
	#load the models
		model = RNN()
		custom_objects={
			'f1_score': model.f1_score, 
			'precision': model.precision, 
			'recall': model.recall
		}
		'''

		model for type of feedback

		'''
		self.tof_model = keras.models.load_model('app/api/v1/ml_models/layer1_tof.h5', custom_objects=custom_objects)
		'''

		model for category

		'''
		self.complaint_model = keras.models.load_model('app/api/v1/ml_models/layer2_complaint.h5', custom_objects=custom_objects)
		self.inquiry_model = keras.models.load_model('app/api/v1/ml_models/layer2_inquiry.h5', custom_objects=custom_objects)
		self.request_model = keras.models.load_model('app/api/v1/ml_models/layer2_request.h5', custom_objects=custom_objects)
		'''

		models for complaint

		'''
		self.complaint_airport_experience_model = keras.models.load_model('app/api/v1/ml_models/layer3_complaint_airport_experience.h5', custom_objects=custom_objects)
		self.complaint_booking_experience_model = keras.models.load_model('app/api/v1/ml_models/layer3_complaint_booking_experience.h5', custom_objects=custom_objects)
		self.complaint_flight_disruption_model = keras.models.load_model('app/api/v1/ml_models/layer3_complaint_flight_disruption.h5', custom_objects=custom_objects)
		self.complaint_getgo_model = keras.models.load_model('app/api/v1/ml_models/layer3_complaint_getgo.h5', custom_objects=custom_objects)
		self.complaint_inflight_experience_model = keras.models.load_model('app/api/v1/ml_models/layer3_complaint_inflight_experience.h5', custom_objects=custom_objects)
		self.complaint_refund_model = keras.models.load_model('app/api/v1/ml_models/layer3_complaint_refund.h5', custom_objects=custom_objects)
		'''

		models for inquiry

		'''
		self.inquiry_booking_inquiry_model = keras.models.load_model('app/api/v1/ml_models/layer3_inquiry_booking_inquiry.h5', custom_objects=custom_objects)
		self.inquiry_cargo_related_model = keras.models.load_model('app/api/v1/ml_models/layer3_inquiry_cargo_related.h5', custom_objects=custom_objects)
		self.inquiry_manage_booking_model = keras.models.load_model('app/api/v1/ml_models/layer3_inquiry_manage_booking.h5', custom_objects=custom_objects)
		'''

		models for requests
		
		'''
		self.request_common_waiver_model = keras.models.load_model('app/api/v1/ml_models/layer3_request_common_waiver.h5', custom_objects=custom_objects)
		self.request_flight_cancellation_changes_options = keras.models.load_model('app/api/v1/ml_models/layer3_request_flight_cancellation changes_options.h5', custom_objects=custom_objects)
		self.request_itinerary_receipt = keras.models.load_model('app/api/v1/ml_models/layer3_request_itinerary_receipt.h5', custom_objects=custom_objects)
		self.request_name_correction = keras.models.load_model('app/api/v1/ml_models/layer3_request_name_correction.h5', custom_objects=custom_objects)
		self.request_official_receipt = keras.models.load_model('app/api/v1/ml_models/layer3_request_official_receipt.h5', custom_objects=custom_objects)
		self.request_passenger_contact_details = keras.models.load_model('app/api/v1/ml_models/layer3_request_official_receipt.h5', custom_objects=custom_objects)
		
		return self.tof_model, self.complaint_model, self.inquiry_model, 
		self.request_model, self.complaint_airport_experience_model, 
		self.complaint_booking_experience_model, self.complaint_flight_disruption_model, 
		self.complaint_getgo_model, self.complaint_inflight_experience_model,
		self.complaint_refund_model, self.inquiry_booking_inquiry_model, self.inquiry_cargo_related_model,
		self.inquiry_manage_booking_model, self.request_common_waiver_model, self.request_flight_cancellation_changes_options,
		self.request_itinerary_receipt, self.request_name_correction, self.request_official_receipt, 
		self.request_passenger_contact_details


	def tokenize():

		tokenizer = Tokenizer(file_output=('app/api/v1/tokenizers/tokenizer'))
		tokenizer.load_tokenizer_from_file()
		tokenize = tokenizer.get_tokenizer()
		return tokenize