from flask import Blueprint, request, jsonify
from flask_restplus import Api, Resource
from .classifier_first_layer import predict

api = Blueprint('classify_api',__name__)

@api.route("/classifications/", methods=['POST'])
def post():
    """
    returns language classified
    """
    data = request.get_json()
    text = data['text']
    predicted = predict(text)
    return jsonify(success=True, data=predicted)