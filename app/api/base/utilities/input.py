#Regex and character pre-processing
import re
import nltk
from nltk.corpus import stopwords
import fasttext
import numpy as np
import time
from tensorflow import keras
import pickle
import gzip

class Sanitizer:
    def __init__(self):
        print("Attempting to download stopwords")
        nltk.download('stopwords')
        file = open('stopwords.txt', 'r')
        contents = file.readlines()
        print("Done downloading stopwords")

        print("Compiling regex values")
        new_line_regex = re.compile(r"\n")

        self.tagalog_stop_words_list = [new_line_regex.sub(" ", content) for content in contents]
        self.english_stop_words_list = set(stopwords.words('english'))

        self.twitter_handle_regex = re.compile(r"@[\w\d]+")
        self.hi_word_regex = re.compile("\bhi\b")
        self.special_characters_regex = re.compile(r"[\[\]^&?.*\"\',:\/\(\)]")
        self.https_regex = re.compile(r'''(?i)\b((?:https?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'".,<>?Â«Â»â€œâ€â€˜â€™]))''')
        self.twitter_links = re.compile(r'''(?i)\bhttps?://t.co/..........''')
        self.all_numbers_regex = re.compile(r"\b(\d*)\b")
        self.ticket_substitute = re.compile(r"(\s)(5j \w+)|(5j)")
        self.date_substitute = re.compile(r"(\d\d(\/|-)\d\d\d\d)|(\d\d(\/|-)\d\d(\/|-)\d\d\d\d)")
        self.time_substitute = re.compile(r"\b(\d\d|\d):(\d\d)(am|pm)\b") #e.g 12:35am
        self.incomplete_time_substitute = re.compile(r"\b(\d\d)(am|pm)\b") #e.g. 10am
        
        print("Done compiling regex values")
        
    def sanitize(self, input_to_sanitize):
        message = input_to_sanitize.lower().strip()
        message = self.hi_word_regex(" ", message)
        message = self.https_regex.sub(" ", message)
        message = self.twitter_handle_regex.sub(" ", message)
        message = self.ticket_substitute.sub(" ticket", message)
        message = self.date_substitute.sub(" date", message)
        message = self.time_substitute.sub(" time", message)
        message = self.incomplete_time_substitute(" time", message)
        message = self.all_numbers_regex.sub(" ",message)
        message = self.twitter_links.sub(" ", message)
        message = self.special_characters_regex.sub(" ", message)
        message = self.twitter_links.sub(" ", message)
        message = self.special_characters_regex.sub(" ", message)
        
        message = ' '.join([word for word in message.split() if word not in self.tagalog_stop_words_list])
        message = ' '.join([word for word in message.split() if word not in self.english_stop_words_list])
        
        return message
    
class Tokenizer:    
    def __init__(self, texts=None, max_words=8000, file_output='tokenizer'):
        self.texts = None
        self.sequences = None
        self.tokenize = None
        self.file_output = file_output + ".pickle"
        if texts is not None:
            self.tokenize = keras.preprocessing.text.Tokenizer(num_words=max_words,char_level=False)
            self.texts = texts
            self.tokenize.fit_on_texts(texts)
            
            with open(self.file_output, 'wb') as handle:
                pickle.dump(self.tokenize, handle, protocol=pickle.HIGHEST_PROTOCOL)
                print("Saved tokenizer to file")
            self.sequences = self.texts_to_sequences()
        else:
            #test loading
            with open(self.file_output, 'rb') as handle:
                self.tokenize = pickle.load(handle)
                print("Load testing successful")
            
    def fit_on_texts(self, texts):
        print("Fitting on texts")
        self.tokenize.fit_on_texts(texts) # fit tokenizer to our data
        # saving
        with open(self.file_output, 'wb') as handle:
            pickle.dump(self.tokenize, handle, protocol=pickle.HIGHEST_PROTOCOL)
            print("Saved tokenizer to file")
        #test loading
        with open(self.file_output, 'rb') as handle:
            test_token = pickle.load(handle)
        print("Load testing successful")
            
    def get_tokenizer(self):
        return self.tokenize
                
    def load_tokenizer_from_file(self):
        # loading
        with open(self.file_output, 'rb') as handle:
            self.tokenize = pickle.load(handle)
        
    def texts_to_sequences(self):
        sequences = self.tokenize.texts_to_sequences(self.texts)
        return sequences
    
    def pad_data_sequences(sequences, max_sequences_length):
        """
        Add padding to the sequence array based on the maximum sequence length

        :param sequences: The batch of data to be padded
        :param max_sequences_length: The shape of the sequence to after padding
        """
        data = keras.preprocessing.sequence.pad_sequences(sequences, maxlen=max_sequences_length)

        return data
            
    def get_word_index(self):
        return self.tokenize.word_index
    
class Embedding:
    def __init__(self, word_index, embedding_dim=300):
        self.model = None #our own trained model from the dataset
        self.embeddings_index = {}
        self.word_index = word_index
        self.EMBEDDING_DIM = embedding_dim
        self.embedding_matrix = None
        
    def train_model(self,file_location='data.txt', fastText_model_name='fasttext_model'):
        print("Training model")
        start_time = time.time()
        
        self.model = fasttext.train_unsupervised(file_location, dim=self.EMBEDDING_DIM, model='skipgram')
        self.model.save_model("{}.bin".format(fastText_model_name))
        
        end_time = time.time() - start_time
        print("Finished training in {}".format(end_time))
        
        print("Loading fasttext trained model")
        for word in self.model.words:
            coefs = np.asarray(self.model[word], dtype='float32')
            self.embeddings_index[word] = coefs
        print('Total %s word vectors' % len(self.embeddings_index))

    def build_embedding(self):
        print("Loading embeddings index into the matrix")
        
        if not self.embeddings_index:#Check if user has loaded the models into the index
            raise Exception('Embeddings indxe is empty current_value: {}'.format(embeddings_index))
            
        self.embedding_matrix = np.random.random((len(self.word_index) + 1, self.EMBEDDING_DIM))
        print("EMBED MATRIX SHAPE {}".format(self.embedding_matrix.shape))
        for word, i in self.word_index.items():
            embedding_vector = self.embeddings_index.get(word)
            if embedding_vector is not None:
                # words not found in embedding index will be all-zeros.
                self.embedding_matrix[i] = embedding_vector
        print('Total %s word embeddings' % len(self.embedding_matrix))
        
    def load_model(self, model_path):
        print("Loading {}".format(model_path))
        start_time = time.time()
#         f = open('fasttext_models/glove.tl.39M.300d.30Kv.txt', 'rb')
        f = open(model_path, 'rb')
        for line in f:
            values = line.split()
            word = values[0]
            coefs = np.asarray(values[1:], dtype='float32')
            self.embeddings_index[word] = coefs
        f.close()
        
#         print("Loading tagalog fastText model")
#         start_time = time.time()
#         f = gzip.open('fasttext_models/cc.tl.300.vec.gz', 'rb')
#         for line in f:
#             values = line.split()
#             word = values[0]
#             coefs = np.asarray(values[1:], dtype='float32')
#             self.embeddings_index[word] = coefs
#         f.close()
#         print("Finished loading at {}".format(time.time()-start_time))
        
#         print("Loading english fastText model")
#         start_time = time.time()
#         f = gzip.open('fasttext_models/cc.en.300.vec.gz', 'rb')
#         for line in f:
#             values = line.split()
#             word = values[0]
#             coefs = np.asarray(values[1:], dtype='float32')
#             self.embeddings_index[word] = coefs
#         f.close()
#         print("Finished loading at {}".format(time.time()-start_time))
        print("Finished loading at {}".format(time.time() - start_time))
        
    def get_embeddings_matrix(self):
        return self.embedding_matrix
    
    def get_model(self):
        return self.model