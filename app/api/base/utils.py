from tensorflow import keras
from .models.RNNModel import RNN
import pickle
from .utilities.input import Tokenizer
MODEL_NAME = 'layer1_tof'
def load_class_index():
# set the class indeces
	class_index = {
		"Inquiry": 0,
		"Complaint": 1,
		"Request": 2,
	}
	return class_index
def model_load():
#load the model
	model = RNN()
	saved_model = keras.models.load_model('app/api/base/ml_models/{}.h5'.format(MODEL_NAME), custom_objects={'f1_score': model.f1_score, 'precision': model.precision, 'recall': model.recall})
	return saved_model

def tokenize():
#load the tokenize
	tokenizer = Tokenizer(file_output=('app/api/base/tokenizers/layer1_tof'))
	tokenizer.load_tokenizer_from_file()
	tokenize = tokenizer.get_tokenizer()
	return tokenize