from tensorflow import keras
import numpy as np
from .utils import *

def predict(input_data):
#prediction test
	class_index = load_class_index()
	MAX_SEQUENCE_LENGTH = 1000
	tokenizer = tokenize()
	mod_load = model_load()
	test_sequence = tokenizer.texts_to_sequences([input_data])
	test_sequence = keras.preprocessing.sequence.pad_sequences(test_sequence, MAX_SEQUENCE_LENGTH)

	pred = mod_load.predict(test_sequence)
	predicted = ([key for key, val in class_index.items() if val == np.argmax(pred)]) #Need ung class index na variable para dito
	return predicted