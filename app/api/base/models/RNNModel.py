"""1 Bidirectional RNN Layers With LSTM"""
import tensorflow as tf
from tensorflow import keras
import time
from datetime import datetime

import os

from keras import backend as K

from .callbacks import LogFilePrintingCallback, EarlyStoppingAtMinLoss, Metrics, LossHistory
from keras.callbacks import TensorBoard

layers = keras.layers
models = keras.models

class RNN:
    def __init__(self, learning_rate=0.0001, batch_size=512, epochs=20, num_classes=None, max_sequence_length=1000,
                 embedding_size=100000,
                 embedding_dim=300,
                 embedding_matrix=None,
                 model_auto_save=True,
                 model_name=None,
                 with_strategy=False,
                 compile_model=True,
                 model_dir='ml_models',
                 memory_limit=None,
                 target_gpu = 0
                ):
        """
        Initializes the RNN Model
        
        :param learning_rate: The learning rate to be used by the model
        :param batch_sze: The number of batches to be used for the training/validation/testing dataset
        :param epochs: The number of epochs to be used by the model
        :param num_classes: The number of classes to be used in the output layer
        :param max_sequence_length: The maximum length of an input vector
        """
        self.learning_rate = learning_rate
        self.batch_size = batch_size
        self.epochs = epochs
        self.num_classes = num_classes
        self.max_sequence_length = max_sequence_length
        self.default_model_name = 'cebpac_cnn'
        self.model_name = model_name if model_name is not None else self.default_model_name
        self.model_name = "{}.h5".format(self.model_name)
        self.model_checkpoint_callback = None
        self.model_auto_save = model_auto_save
        self.embedding_size = embedding_size
        self.embedding_matrix = embedding_matrix
        self.embedding_dim = embedding_dim
        self.model = None
        self.model_dir = model_dir
        self.history = LossHistory()
        self.with_strategy = with_strategy
        self.memory_limit = memory_limit
#         tf.debugging.set_log_device_placement(True)

#         if compile_model:
#             self.model = self.build_model()
            
        if self.model_name is not None:
            if not os.path.exists(self.model_dir):#Check if the current directory exists
                os.makedirs(self.model_dir)
                
                #Set growth limit
#         print("Setting Growth Memory Limit")
#         gpu_list = tf.config.experimental.list_physical_devices('GPU')
#         print("Found GPUs: {}".format(gpu_list))
#         if memory_limit is not None:
#             tf.config.experimental.set_memory_growth(gpu_list[0], True)
#         tf.config.experimental.set_virtual_device_configuration(gpu_list[0],[tf.config.experimental.VirtualDeviceConfiguration(memory_limit=self.memory_limit)])

        self.model_checkpoint_callback=keras.callbacks.ModelCheckpoint(self.model_dir+"/"+self.model_name,monitor='val_categorical_accuracy',verbose=1,save_best_only=True)
        
    def build_model(self, custom_model=None, summary=False):
        """
        Builds the model
        """
        """
        If Distributed training is enabled
        """
        #Model attribute instantiation validation
        print("Building Model")

        #Check if instance has GPUs
        print("GPU Availability {}".format(tf.test.is_gpu_available()))

        if custom_model is None:
#             with central_storage_strategy.scope():
                with tf.device('/cpu:0'):
                    model = tf.keras.Sequential([
                        keras.layers.Input(shape=(self.max_sequence_length,)),
                        keras.layers.Embedding(self.embedding_size,
                                                            self.embedding_dim,
                                                            weights=[self.embedding_matrix],
                                                            input_length=self.max_sequence_length,
                                                            trainable=False),
                        keras.layers.Bidirectional(keras.layers.LSTM(156, dropout=0.5)),
                        keras.layers.Dense(self.num_classes, activation='softmax')
                    ])

                if summary:
                    print(model.summary())
                
#                 model = tf.keras.utils.multi_gpu_model(model, gpus=2)
                model.compile(
                    loss='categorical_crossentropy',
                    optimizer=tf.optimizers.Adam(learning_rate=0.001),   
                    metrics=[
                        'categorical_accuracy',
                        self.f1_score,
                        self.precision,
                        self.recall
                    ]
                )
        else:
            model = custom_model
            
        self.model = model
#         return model

    def summary(self):
        self.model.summary()
    
    def fit(self, x_train, y_train, x_val, y_val, x_test=None, y_test=None):
        """
        Trains the initialized model
            
        :param x_train: Features of the training dataset
        :param y_train: Labels of the training dataset
        :param x_val: Features of the validation dataset
        :param y_val: Labels of the validation dataset
        :param x_test: Features of the test dataset
        :param y_test: Labels of the test dataset
        :return: None
        """
        
        assert self.model is not None, "No Model has been Compiled, will stop fitting"
        assert x_train is not None and y_train is not None and x_val is not None and y_val is not None, "Training and validation sets are required to start training"
        
        evaluate_test = False
        if x_test is not None or y_test is not None:
            assert len(x_test) == len(y_test), "Test features and test labels are not the same length, shapes are {} and {}".format(x_test.shape, y_test.shape)
            evaluate_test = True
            
        callback_list = []
        
        
        if self.model_auto_save: #Auto save model
            callback_list = [self.model_checkpoint_callback]
            
        """
        Create new log file before starting training
        """
        with open('log_file.txt', 'w') as f:
            now = datetime.now()
            f.write("Started training at {}\n".format(now.strftime("%Y-%m-%d %H:%M")))
        f.close()
        
        """
        Call back functions
        """
        callback_list.append(LogFilePrintingCallback())
#         callback_list.append(TensorBoard(log_dir='logs', histogram_freq=1))
        callback_list.append(keras.callbacks.ReduceLROnPlateau(monitor='val_loss', factor=0.1, patience=1, min_lr=0.00001))
    
        self.history = LossHistory()
        callback_list.append(self.history)
#         callback_list.append(Metrics())
#         callback_list.append(EarlyStoppingAtMinLoss())
        
        """
        Training function
        """
        print("Training started")
        start_time = time.time()
        
        if self.with_strategy:
            train_dataset = tf.data.Dataset.from_tensor_slices((x_train,y_train)).shuffle(len(x_train)+1).batch(self.batch_size)
            validation_dataset = tf.data.Dataset.from_tensor_slices((x_val, y_val)).shuffle(len(x_val)+1).batch(self.batch_size)
            self.model.fit(train_dataset, validation_data=validation_dataset, steps_per_epoch=25, epochs=self.epochs, callbacks=callback_list)
        else:
            #Train
            train_dataset = tf.data.Dataset.from_tensor_slices((x_train, y_train))
            train_dataset = train_dataset.prefetch(1024).shuffle(1024).batch(self.batch_size, True)
            #Val
            validation_dataset = tf.data.Dataset.from_tensor_slices((x_val, y_val))
            validation_dataset = validation_dataset.prefetch(1024).shuffle(1024).batch(self.batch_size, True)
            
            self.model.fit(train_dataset, validation_data=validation_dataset, epochs=self.epochs, callbacks=callback_list)
#             self.model.fit(x_train,y_train, validation_data=(x_val, y_val),epochs=self.epochs, batch_size=self.batch_size, callbacks=callback_list)
        print("--- %s seconds ---" % (time.time() - start_time))
        
        """
        Evaluation
        """
        if evaluate_test:
            dataset = tf.data.Dataset.from_tensor_slices((x_test,y_test)).shuffle(10).batch(2048)
            if self.with_strategy:
                self.evaluate(dataset=dataset, with_strategy=True)
            else:
                self.evaluate(x_test, y_test)
            
    def save_model(self, directory="ml_models"):
        """
        Saves the model using the specified model name, if not specified, it will use a default model name
        """
        print("Attempting to save model")
        if not os.path.exists(directory):#Check if the current directory exists
            os.makedirs(directory)
        model_file_name = self.model_name.replace("/", "")
        self.model.save(directory+"/"+model_file_name)
        print("Model Saved")
        
    def evaluate(self, x_test=None, y_test=None, dataset=None, with_strategy=False):
        """
        Evaluates the model with the test dataset
        
        :param x_test: Features of the test dataset
        :param y_test: Labels of the test dataset
        """        
        if with_strategy:
            assert dataset is not None, "If distributed training is enabled, you must use dataset parameter"
            
            results = self.model.evaluate(dataset)
        else:
            assert x_test is not None and y_test is not None
            
            print("No strategy enabled, starting evaluation")
            results = self.model.evaluate(x_test, y_test, batch_size=512)
            print("Evaluation results: {}".format(results))
            
    def recall(self, y_true, y_pred):
        true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
        possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
        recall = true_positives / (possible_positives + K.epsilon())
        return recall

    def precision(self, y_true, y_pred):
        true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
        predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
        precision = true_positives / (predicted_positives + K.epsilon())
        return precision

    def f1_score(self, y_true, y_pred):
        precision = self.precision(y_true, y_pred)
        recall = self.recall(y_true, y_pred)
        return 2*((precision*recall)/(precision+recall+K.epsilon()))