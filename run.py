#!/usr/bin/env python
#
# Copyright 2019 Senti TechLabs, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# =========================================================================

from flask import Flask

app = Flask(__name__)

from app.api.v1.routes import api as v1
# from app.api.v1.utils import Load

# load = Load()
# tof_index,inquiry_index,complaint_index,request_index = load.load_index()
# tof_model, complaint_model, inquiry_model, request_model = load.load_model()
# tokenize = load.tokenize()
app.register_blueprint(v1, url_prefix='/api/v1/')

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080, debug=True)
